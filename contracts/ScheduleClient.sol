pragma solidity 0.4.24;

import './usingOraclize.sol';
import './IERC20.sol';

contract ScheduleClient is usingOraclize {
    address private _owner;
    address private _controller;

    uint256 private _id;

    uint256 private constant _gasPrice = 16000000000; // gas price is 16 gwei by default
    uint256 private constant _gasLimit = 140000;      // gas limit is 140,000 by default

    // the all hashes of schedules
    bytes32[] private _schedules;

    IERC20 private constant _token = IERC20(0x2b348aAe30E1183Cf98ac6E23AaEF79B52717846);

    struct ScheduleCall {
        address receiver;
        uint256 delay;
        uint256 executed;
        uint256 executionsAmount;
        uint256 tokensPerDelay;
        bool canceled;
        uint256 creationDate;
    }

    mapping(bytes32 => bytes32) private _oraclizeCallbacks;  // oraclize_query -> ScheduleCall data hash
    mapping(bytes32 => ScheduleCall) private _scheduleCalls; // ScheduleCall data hash -> ScheduleCall details

    event LogInfo(string description);
    event ScheduleCreated(bytes32 indexed data, address receiver, uint256 delay, uint256 executionsAmount, uint256 tokensPerDelay);

    // -----------------------------------------
    // CONSTRUCTOR
    // -----------------------------------------

    constructor (address owner) public {
        _controller = msg.sender;
        _owner = owner;
        oraclize_setCustomGasPrice(_gasPrice);
    }

    // -----------------------------------------
    // EXTERNAL
    // -----------------------------------------

    function addScheduleCall(address receiver, uint256 delay, uint256 executionsAmount, uint256 tokensPerDelay) external payable {
        require(msg.sender == _controller, "addScheduleCall: the caller is not the controller contract");
        _createSchedule(receiver, delay, executionsAmount, tokensPerDelay);
    }

    function cancel(bytes32 hash) external {
        require(msg.sender == _owner, 'cancel: the msg.sender is not the owner of this contract');
        ScheduleCall storage call = _scheduleCalls[hash];

        require(call.canceled == false, 'cancel: this schedule was already canceled');
        require(call.executionsAmount > call.executed, 'cancel: there are no active schedule calls');
        _checkCancelAvailability(call.creationDate, call.executed, call.delay);

        uint256 availableCalls = call.executionsAmount - call.executed;
        call.canceled = true;

        require(_token.transfer(msg.sender, availableCalls * call.tokensPerDelay));
        require(_owner.send(availableCalls * _gasLimit * _gasPrice));
    }

    function emergencyExit() external {
        require(msg.sender == _owner, 'emerganceExit: the msg.sender is not the owner of this contract');
        require(_token.transfer(_owner, _token.balanceOf(address(this))));
        require(_owner.send(address(this).balance));
    }

    function getScheduleDetails(bytes32 hash) external view returns (address, uint256, uint256, uint256, uint256, bool, uint256) {
        return (
            _scheduleCalls[hash].receiver,
            _scheduleCalls[hash].delay,
            _scheduleCalls[hash].executed,
            _scheduleCalls[hash].executionsAmount,
            _scheduleCalls[hash].tokensPerDelay,
            _scheduleCalls[hash].canceled,
            _scheduleCalls[hash].creationDate
        );
    }

    function getId() external view returns (uint256) {
        return _id;
    }

    function schedules(uint256 id) external view returns (bytes32) {
        return _schedules[id];
    }

    // -----------------------------------------
    // EXECUTION CALLBACK
    // -----------------------------------------

    function __callback(bytes32 myid, string result) public {
        require(msg.sender == oraclize_cbAddress(), "__callback: only the Oraclize registered address can execute functions");
        ScheduleCall memory call = _scheduleCalls[ _oraclizeCallbacks[myid] ];

        if(call.executionsAmount > call.executed && call.canceled == false) {
            address receiver = call.receiver;
            uint256 delay = call.delay;
            uint256 tokensPerDelay = call.tokensPerDelay;

            if (_token.balanceOf(address(this)) < tokensPerDelay) {
                emit LogInfo("Not enough balance");
            } else {
                _scheduleCalls[_oraclizeCallbacks[myid]].executed += 1;
                require(_token.transfer(receiver, tokensPerDelay), "__callback: the tokens was not sent");

                // if there are available calls
                if(_scheduleCalls[_oraclizeCallbacks[myid]].executionsAmount > _scheduleCalls[_oraclizeCallbacks[myid]].executed) {
                    // Generating unique query for oraclize
                    bytes32 queryId = oraclize_query(delay, "", "", _gasLimit);
                    _oraclizeCallbacks[queryId] = _oraclizeCallbacks[myid];
                }
            }
        }
    }

    // -----------------------------------------
    // INTERNAL
    // -----------------------------------------

    function _createSchedule(address receiver, uint256 delay, uint256 executionsAmount, uint256 tokensPerDelay) internal {
        bytes32 data = _getHash(receiver, delay, executionsAmount, tokensPerDelay, _id);
        _id += 1;

        ScheduleCall memory call = ScheduleCall(receiver, delay, 0, executionsAmount, tokensPerDelay, false, block.timestamp);
        _scheduleCalls[data] = call;

        // Generating unique query for this schedullCall with oraclize
        bytes32 queryId = oraclize_query(delay, "", "", _gasLimit);
        _oraclizeCallbacks[queryId] = data;

        _schedules.push(data);

        emit ScheduleCreated(data, receiver, delay, executionsAmount, tokensPerDelay);
    }

    function _getHash(address receiver, uint256 delay, uint256 executionsAmount, uint256 tokensPerDelay, uint256 id) internal pure returns (bytes32) {
        return keccak256(abi.encodePacked(receiver, delay, executionsAmount, tokensPerDelay, id));
    }

    function _checkCancelAvailability(uint256 creationDate, uint256 executed, uint256 delay) internal view {
        require(
            block.timestamp < creationDate + ((executed + 1) * delay) - 2 minutes ||
            block.timestamp > creationDate + ((executed + 1) * delay) + 2 minutes,
            '_checkCancelAvailability: you cannot cancel schedule call +- 2 minutes from current and last scheduled call'
        );
    }

    // -----------------------------------------
    // FALLBACK
    // -----------------------------------------

    function () public payable {
        // ETH claimed
    }
}
